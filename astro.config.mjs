import { defineConfig } from 'astro/config';

export default defineConfig({
    sitemap: true,
    outDir: "public",
    publicDir: "static",
    site: "https://somaxa8.gitlab.io",
    base: "/personal-web"
});
